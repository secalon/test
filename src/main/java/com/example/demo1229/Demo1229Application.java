package com.example.demo1229;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo1229Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo1229Application.class, args);
    }

}
